from random import randint
from pieces.pawn import Pawn, promotable_pieces
from chess_board import ChessBoard
from chess_move import ChessMove
from chess_game import ChessGame
from copy import deepcopy
import time

def ai_move(game: ChessGame):
    '''Makes a random move.'''
    moves = game.get_current_moves()
    if len(moves) > 0:
        r = randint(0, len(moves)-1)
        move = moves[r]
        game.move(move)
    # Here is how you would look at the next board position
    # next_pos = deepcopy(game)
    # other_team_next_moves = next_pos.get_current_moves()
    # if len(other_team_next_moves) > 0:
    #     r = randint(0, len(other_team_next_moves)-1)
    #     move = other_team_next_moves[r]
    #     game.move(move)
    
