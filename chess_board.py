from pieces.chess_piece import ChessPiece
from teams import other_team
from typing import Callable

class ChessBoard:
    def __init__(self, size: tuple[int,int]=(8,8)):
        self.__board = []
        for rank in range(size[0]):
            column = []
            for file in range(size[1]):
                column.append(None)
            self.__board.append(column)
    
    def get_piece(self, rank, file) -> ChessPiece | None:
        return self.__board[rank][file]

    def set_piece(self, rank, file, piece):
        self.__board[rank][file] = piece

    def size(self) -> tuple[int,int]:
        ranks = len(self.__board)
        files = len(self.__board[0])
        return (ranks, files)

    def set_board(self, pieces: list[list[ChessPiece]]):
        self.__board = pieces

    def is_on_board(self, pos):
        return (
            0 <= pos[0] < len(self.__board)
            and 0 <= pos[1] < len(self.__board[0])
        )

    def move(self, start: tuple[int, int], end: tuple[int, int]):
        start_rank, start_file = start
        end_rank, end_file = end
        piece = self.get_piece(start_rank, start_file)

        # Check if the start position is valid and has a piece
        if (self.is_on_board(start) and piece is not None):
            piece = self.__board[start_rank][start_file]

            # Check if the end position is valid
            if self.is_on_board(end):
                # Move the piece to the end position
                self.__board[end_rank][end_file] = piece
                self.__board[start_rank][start_file] = None

    def in_check(self, team):
        for i in range(self.size()[0]):
            for j in range(self.size()[1]):
                piece = self.get_piece(i, j)
                if piece and piece.checkable and piece.color == team:
                    if piece.is_in_check(self, (i,j)):
                        return True
        return False

    def checkmated(self, team):
        if not self.in_check(team):
            return False
        for i in range(self.size()[0]):
            for j in range(self.size()[1]):
                piece = self.get_piece(i, j)
                if piece and piece.color == team:
                    moves = piece.get_legal_moves(self, (i, j))
                    if len(moves) > 0:
                        return False
        return True

    def stalemated(self, team):
        if self.in_check(team):
            return False
        for i in range(self.size()[0]):
            for j in range(self.size()[1]):
                piece = self.get_piece(i, j)
                if piece and piece.color == team:
                    moves = piece.get_legal_moves(self, (i, j))
                    if len(moves) > 0:
                        return False
        return True