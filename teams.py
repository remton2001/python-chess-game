B = 'black'
W = 'white'

def other_team(team):
    return B if team == W else W
