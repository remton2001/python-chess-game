from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from chess_board import ChessBoard

class ChessMove():
    """Class for holding information about a chess move

        board: the new board position produced by the move
        start: the move's starting position
        end: the move's ending position
    """
    def __init__(self, 
                 board: "ChessBoard", 
                 start: tuple[int,int], 
                 end: tuple[int,int]):
        self.board = board
        self.start = start
        self.end = end
        self.moved_piece = board.get_piece(end[0],end[1])
