from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from board import ChessBoard

from pieces.chess_piece import *
from pieces.rook import Rook




class King(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_king.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_king.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)
    
    def __init__(self, color):
        super().__init__(color, "King")
        self.has_moved = False
        self.checkable = True

    def is_in_check(self, board: "ChessBoard", my_pos: tuple[int,int]):
        for i in range(board.size()[0]):
            for j in range(board.size()[1]):
                piece = board.get_piece(i, j)
                if piece and piece.color != self.color:
                    moves = piece.get_moves(board, (i, j))
                    for move in moves:
                        if move.end == my_pos:
                            return True
        return False

    def get_legal_moves(self, board: "ChessBoard", my_pos: tuple[int,int]) -> list[ChessMove]:
        moves = super().get_legal_moves(board, my_pos)
        my_rank = my_pos[0]
        my_file = my_pos[1]
        # Castling
        def puts_in_check(board: "ChessBoard", start, end):
            new_board = deepcopy(board)
            new_board.move(start, end)
            return self.is_in_check(new_board, end)

        if not self.has_moved and not self.is_in_check(board, my_pos):
            # right castling
            for i in range(1, 4, 1):
                file = my_file + i
                if puts_in_check(board, my_pos, (my_rank, file)):
                    break
                if board.is_on_board((my_rank, file)):
                    other = board.get_piece(my_rank, file)
                    if other:
                        if (isinstance(other, Rook) 
                            and other.color == self.color 
                            and not other.has_moved
                        ):
                            new_board = deepcopy(board)
                            new_board.move(my_pos, (my_rank, my_file + 2))
                            new_board.move((my_rank,my_file+3),(my_rank,my_file+1))
                            moves.append(ChessMove(new_board, my_pos, (my_rank, my_file + 2)))
                        else:
                            break
            # left castling
            for i in range(1, 5, 1):
                file = my_file - i
                if board.is_on_board((my_rank, file)):
                    other = board.get_piece(my_rank, file)
                    if puts_in_check(board, my_pos, (my_rank, my_file - i)):
                        break
                    if other:
                        if (isinstance(other, Rook) 
                            and other.color == self.color 
                            and not other.has_moved
                        ):
                            new_board = deepcopy(board)
                            new_board.move(my_pos, (my_rank, my_file - 2))
                            new_board.move((my_rank,my_file-4),(my_rank,my_file-1))
                            moves.append(ChessMove(new_board, my_pos, (my_rank, my_file - 2)))
                        else:
                            break
        return moves

    def get_moves(self, board: "ChessBoard", my_pos: tuple[int,int]) -> list[ChessMove]:
        '''Returns a list of legal ending positions'''
        my_rank = my_pos[0]
        my_file = my_pos[1]
        moves = []
        # Normal moves
        for rank in range(max(0, my_rank-1), min(board.size()[0], my_rank+2)):
            for file in range(max(0, my_file-1), min(board.size()[1], my_file+2)):
                other: ChessPiece = board.get_piece(rank, file)
                if other and other.color == self.color:
                    continue
                new_board = deepcopy(board)
                new_board.move(my_pos, (rank,file))
                moves.append(ChessMove(new_board, my_pos, (rank,file)))
        return moves
    
    def on_move(self, board: "ChessBoard", 
                moved_piece: ChessPiece, 
                start:tuple[int,int], 
                end:tuple[int,int]):
        if moved_piece == self:
            self.has_moved = True  
