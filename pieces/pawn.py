from typing import TYPE_CHECKING
from chess_move import ChessMove
if TYPE_CHECKING:
    from chess_board import ChessBoard

from pieces.chess_piece import *
import pygame
from pygame.locals import *

from easygui import choicebox, buttonbox

from pieces.knight import Knight
from pieces.queen import Queen
from pieces.bishop import Bishop
from pieces.rook import Rook

def promotable_pieces(color) -> list[ChessPiece]:
    return [
        Queen(color),
        Rook(color),
        Bishop(color),
        Knight(color),
    ]

def get_promotion(color):
    # choices which user can select
    
    pieces = promotable_pieces(color)
    
    title = "Promote pawn"
    msg = ""
    get_image = lambda x: x.WHITE_ICON_FILE if color == W else x.BLACK_ICON_FILE
    images = [get_image(x) for x in pieces]
    choice = buttonbox(msg=msg, title=title, choices=[], images=images)
    print(choice)
    for piece in pieces:
        if choice == get_image(piece):
            return piece
    return pieces[0]


class Pawn(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_pawn.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_pawn.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)

    def __init__(self, color):
        super().__init__(color, "Pawn")
        self.has_moved = False
        self.can_passant = False

    def ready_to_promote(self, board, my_pos):
        return (
            (self.color == W and my_pos[0] == 0) 
            or (self.color == B and my_pos[0] == board.size()[0]-1)
        )

    def on_move(self, board: "ChessBoard", 
                moved_piece: ChessPiece, 
                start: tuple[int,int], 
                end: tuple[int,int]):
        if moved_piece == self:
            self.has_moved = True

        # update if the move enables en passant
        if moved_piece == self and abs(end[0] - start[0]) > 1:
            self.can_passant = True
        else:
            self.can_passant = False

        # Promotion
        if moved_piece == self:
            # white promotes
            if (self.ready_to_promote(board, end)):
                promoted_piece = get_promotion(self.color)

                # Replace the promoted pawn with the selected piece
                board.set_piece(end[0], end[1], promoted_piece)
    

    def get_legal_moves(self, board, my_pos, promotions_as_different_moves=True) -> list[ChessMove]:
        moves = self.get_moves(board, my_pos, promotions_as_different_moves)
        # Check if moves leave us in check
        moves = [move for move in moves if not move.board.in_check(self.color)]
        return moves

    def get_moves(self, board: "ChessBoard", my_pos, promotions_as_different_moves=True) -> list[ChessMove]:
        '''Returns a list of legal ending positions'''
        moves: list[ChessMove] = []
        my_rank = my_pos[0]
        my_file = my_pos[1]
        forward = -1 if self.color == W else 1

        # move forward
        rank = my_rank + forward
        if board.is_on_board((rank, my_file)):
            other = board.get_piece(rank, my_file)
            if not other:
                # move 1 forward
                new_board = deepcopy(board)
                new_board.move(my_pos, (rank, my_file))
                moves.append(ChessMove(new_board, my_pos, (rank, my_file)))
                if not self.has_moved:
                    rank = my_rank + forward * 2
                    if board.is_on_board((rank, my_file)):
                        other = board.get_piece(rank, my_file)
                        if not other:
                        # move 2 forward
                            new_board = deepcopy(board)
                            new_board.move(my_pos, (rank, my_file))
                            moves.append(ChessMove(new_board, my_pos, (rank, my_file)))

        # Take pieces
        rank = my_rank + forward
        for i in [-1,1]:
            file = my_file + i
            if board.is_on_board((rank, file)):
                other = board.get_piece(rank, file)
                if other and other.color != self.color:
                    new_board = deepcopy(board)
                    new_board.move(my_pos, (rank, file))
                    moves.append(ChessMove(new_board, my_pos, (rank, file)))
        
        # en passant 
        for i in [-1,1]:
            file = my_file + i
            rank = my_rank
            if board.is_on_board((rank, file)):
                other = board.get_piece(rank, file)
                if other and isinstance(other, Pawn) and other.color != self.color and other.can_passant:
                    new_board = deepcopy(board)
                    new_board.set_piece(rank, file, None)
                    new_board.move(my_pos, (rank + forward, file))
                    moves.append(ChessMove(new_board, my_pos, (rank + forward, file)))

        if promotions_as_different_moves:
            new_moves = []
            for move in moves:
                move: ChessMove
                if move.moved_piece.ready_to_promote(move.board, move.end):
                    for p in [Knight(self.color), 
                              Rook(self.color), 
                              Queen(self.color), 
                              Bishop(self.color)]:
                        new_board = deepcopy(move.board)
                        new_board.set_piece(move.end[0], move.end[1], p)
                        new_moves.append(ChessMove(new_board, move.start, move.end))
                    moves.remove(move)
            moves += new_moves
        return moves
