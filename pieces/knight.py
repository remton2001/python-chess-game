from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from board import ChessBoard

from pieces.chess_piece import *

class Knight(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_knight.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_knight.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)
    
    
    
    def __init__(self, color):
        super().__init__(color, "Knight")

    def get_moves(self, board: "ChessBoard", my_pos: tuple[int,int]) -> list[ChessMove]:
        '''Returns a list of legal ending positions'''
        moves = []
        my_rank = my_pos[0]
        my_file = my_pos[1]

        offsets = [
            (-1,2),
            (-1,-2),
            (1,2),
            (1,-2),
            (-2,1),
            (-2,-1),
            (2,1),
            (2,-1),
        ]
        for offset in offsets:
            rank = my_rank + offset[0]
            file = my_file + offset[1]
            if not board.is_on_board((rank,file)):
                continue
            other: ChessPiece = board.get_piece(rank, file)
            if not other or (other and other.color != self.color):
                new_board = deepcopy(board)
                new_board.move(my_pos, (rank, file))
                moves.append(ChessMove(new_board, my_pos, (rank, file)))
        return moves
