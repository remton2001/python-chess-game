import pygame
import os
from copy import deepcopy, copy
from teams import *
from pygame.locals import *
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from chess_board import ChessBoard

from chess_move import ChessMove

class ChessPiece:
    """Parent class for the chess pieces"""
    BLACK_ICON_FILE: str = ""
    WHITE_ICON_FILE: str = ""
    WHITE_ICON: pygame.Surface = None
    BLACK_ICON: pygame.Surface = None

    def __init__(self, color, name):
        self.color = color
        self.name = name
        self.checkable = False
    
    def is_in_check(self, board, my_pos):
        return False

    def get_moves(self, board, my_pos) -> list[ChessMove]:
        pass

    def get_legal_moves(self, board, my_pos) -> list[ChessMove]:
        '''Returns a list of legal moves as new boards positions'''
        moves = self.get_moves(board, my_pos)
        # Check if moves leave us in check
        moves = [move for move in moves if not move.board.in_check(self.color)]
        return moves
    
    def on_move(self, board, moved_piece, start, end):
        pass

