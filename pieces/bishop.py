from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from board import ChessBoard

from pieces.chess_piece import *


class Bishop(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_bishop.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_bishop.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)
    
    def __init__(self, color):
        super().__init__(color, "Bishop")
        
    def get_moves(self, board: "ChessBoard", my_pos: tuple[int,int]) -> list[ChessMove]:
        '''Returns a list of legal ending positions'''
        my_rank = my_pos[0]
        my_file = my_pos[1]
        moves = []
        def _add_move(new_pos):
            new_board = deepcopy(board)
            new_board.move(my_pos, new_pos)
            moves.append(ChessMove(new_board, my_pos, new_pos))
        # up-left
        for i in range(min(my_rank+1, my_file+1)):
            rank = my_rank - i
            file = my_file - i
            other: ChessPiece = board.get_piece(rank, file)
            if other and (rank, file) != (my_rank, my_file):
                if other.color != self.color:
                    _add_move((rank,file))
                break
            if my_pos != (rank,file):
                _add_move((rank,file))
        # down-left
        for i in range(min(board.size()[0] - my_rank, my_file+1)):
            rank = my_rank + i
            file = my_file - i
            other: ChessPiece = board.get_piece(rank, file)
            if other and (rank, file) != (my_rank, my_file):
                if other.color != self.color:
                    _add_move((rank,file))
                break
            if my_pos != (rank,file):
                _add_move((rank,file))
        # up-right
        for i in range(min(my_rank+1, board.size()[0] - my_file)):
            rank = my_rank - i
            file = my_file + i
            other: ChessPiece = board.get_piece(rank, file)
            if other and (rank, file) != (my_rank, my_file):
                if other.color != self.color:
                    _add_move((rank,file))
                break
            if my_pos != (rank,file):
                _add_move((rank,file))
        # down-right
        for i in range(min(board.size()[0] - my_rank, board.size()[0] - my_file)):
            rank = my_rank + i
            file = my_file + i
            other: ChessPiece = board.get_piece(rank, file)
            if other and (rank, file) != (my_rank, my_file):
                if other.color != self.color:
                    _add_move((rank,file))
                break
            if my_pos != (rank,file):
                _add_move((rank,file))
        return moves
