from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from board import ChessBoard

from pieces.chess_piece import *
from pieces.rook import Rook
from pieces.bishop import Bishop


class Queen(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_queen.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_queen.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)

    def __init__(self, color):
        super().__init__(color, "Queen")

    def get_moves(self, board, my_pos) -> list["ChessBoard"]:
        moves = []
        moves += Rook.get_moves(self, board, my_pos)
        moves += Bishop.get_moves(self, board, my_pos)
        return moves
