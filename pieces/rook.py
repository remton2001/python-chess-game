from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from board import ChessBoard

from pieces.chess_piece import *


class Rook(ChessPiece):
    WHITE_ICON_FILE = os.path.join("icons", "white_rook.png")
    BLACK_ICON_FILE = os.path.join("icons", "black_rook.png")
    WHITE_ICON = pygame.image.load(WHITE_ICON_FILE)
    BLACK_ICON = pygame.image.load(BLACK_ICON_FILE)

    def __init__(self, color):
        super().__init__(color, "Rook")
        self.has_moved = False

    def on_move(self, board, moved_piece, start, end):
        if moved_piece == self:
            self.has_moved = True
        
    def get_moves(self, board: "ChessBoard", my_pos) -> list[ChessMove]:
        '''Returns a list of legal ending positions'''
        my_rank = my_pos[0]
        my_file = my_pos[1]
        moves = []
        def _add_move(new_pos):
            new_board = deepcopy(board)
            new_board.move(my_pos, new_pos)
            moves.append(ChessMove(new_board, my_pos, new_pos))
        # down
        for rank in range(my_rank, -1, -1):
            other: ChessPiece = board.get_piece(rank, my_file)
            if other and rank != my_rank:
                if other.color != self.color:
                    _add_move((rank, my_file))
                break
            if my_pos != (rank,my_file):
                _add_move((rank, my_file))
        # up
        for rank in range(my_rank, board.size()[0], 1):
            other: ChessPiece = board.get_piece(rank, my_file)
            if other and rank != my_rank:
                if other.color != self.color:
                    _add_move((rank, my_file))
                break
            if my_pos != (rank,my_file):
                _add_move((rank, my_file))
        # left
        for file in range(my_file, -1, -1):
            other: ChessPiece = board.get_piece(my_rank, file)
            if other and file != my_file:
                if other.color != self.color:
                    _add_move((my_rank, file))
                break
            if my_pos != (my_rank, file):
                _add_move((my_rank, file))
        # right
        for file in range(my_file, board.size()[1], 1):
            other: ChessPiece = board.get_piece(my_rank, file)
            if other and file != my_file:
                if other.color != self.color:
                    _add_move((my_rank, file))
                break
            if my_pos != (my_rank, file):
                _add_move((my_rank, file))
        return moves