import pygame
from chess_board import ChessBoard
from teams import W, B

class ChessDisplay:
    """Class for displaying the chess board"""

    def __init__(self, board: ChessBoard):
        # WINDOW SIZE
        self.WINDOW_WIDTH = 700
        self.WINDOW_HEIGHT = 700    
        self.BOARDER_WIDTH = 20
        self.BOARDER_HEIGHT = 20

        # COLORS
        self.BOARDER_COLOR = (92, 65, 35)
        self.WHITE_SPACE = (242, 240, 211)
        self.BLACK_SPACE = (199, 165, 127)
        self.SELECTED_COLOR = (255, 255, 0)
        self.HIGHLIGHT_WHITE_SPACE = (230, 230, 120)
        self.HIGHLIGHT_BLACK_SPACE = (200, 200, 0)
        self.CHECK_COLOR = (230, 100, 100)

        # Calculate board size
        self.BOARD_TOP_LEFT = (self.BOARDER_WIDTH, self.BOARDER_HEIGHT)
        self.BOARD_BOT_RIGHT = (self.WINDOW_WIDTH-self.BOARDER_WIDTH, 
                                self.WINDOW_HEIGHT-self.BOARDER_HEIGHT)
        self.BOARD_WIDTH = self.BOARD_BOT_RIGHT[0] - self.BOARD_TOP_LEFT[0]
        self.BOARD_HEIGHT =  self.BOARD_BOT_RIGHT[1] - self.BOARD_TOP_LEFT[1]
        self.SQUARE_WIDTH = self.BOARD_WIDTH / board.size()[0]
        self.SQUARE_HEIGHT = self.BOARD_HEIGHT / board.size()[1]

        # Initialize Pygame
        pygame.init()
        self.screen = pygame.display.set_mode((self.WINDOW_WIDTH, self.WINDOW_HEIGHT))
        pygame.display.set_caption("Chess Game")

    # Get the board position given the mouse position
    def board_pos(self, mouse_pos: tuple[int,int]) -> tuple[int,int] | None:
        """Get the board position for the current mouse position"""
        if (   mouse_pos[0] < self.BOARD_TOP_LEFT[0] or mouse_pos[0] > self.BOARD_BOT_RIGHT[0]
            or mouse_pos[1] < self.BOARD_TOP_LEFT[1] or mouse_pos[1] > self.BOARD_BOT_RIGHT[1] 
        ):
            return None
        
        clicked_row = int((mouse_pos[1] - self.BOARD_TOP_LEFT[0]) // self.SQUARE_WIDTH)
        clicked_col = int((mouse_pos[0] - self.BOARD_TOP_LEFT[1]) // self.SQUARE_HEIGHT)
        return (clicked_row, clicked_col)

    def display_message(self, message):
        """Display a message"""
        font_size = int(self.WINDOW_WIDTH / 12)
        font = pygame.font.Font(None, font_size)
        text = font.render(message, True, (230, 230, 230), (30, 30, 30))
        message_size = font.size(message)
        pos = (
            self.WINDOW_WIDTH / 2 - message_size[0] / 2, 
            self.WINDOW_HEIGHT / 3 - message_size[1] / 2
        )
        self.screen.blit(text, pos)

    # Function to draw the chessboard
    def draw_board(self, board: ChessBoard, selected_piece_pos: tuple[int,int] = None):
        """Draw the given chess board to the screen"""
        self.SQUARE_WIDTH = self.BOARD_WIDTH / board.size()[0]
        self.SQUARE_HEIGHT = self.BOARD_HEIGHT / board.size()[1]

        size = board.size()
        selected_piece = None
        if selected_piece_pos: 
            selected_piece = board.get_piece(selected_piece_pos[0], selected_piece_pos[1])
        moves = selected_piece.get_legal_moves(board, selected_piece_pos) if selected_piece else []
        move_end_positions = [move.end for move in moves]
        pygame.draw.rect(self.screen, self.BOARDER_COLOR, 
                        (0, 0, self.WINDOW_HEIGHT, self.WINDOW_WIDTH))
        pygame.draw.rect(self.screen, self.WHITE_SPACE, 
                        (self.BOARD_TOP_LEFT[0], self.BOARD_TOP_LEFT[1], self.BOARD_WIDTH, self.BOARD_HEIGHT))

        for row in range(size[0]):
            for col in range(size[1]):
                color = self.WHITE_SPACE if (row + col) % 2 == 0 else self.BLACK_SPACE
                # Highlight the selected piece's square
                if selected_piece_pos == (row, col):
                    color = self.SELECTED_COLOR
                # Highlight the moves 
                if (row, col) in move_end_positions:
                    color = self.HIGHLIGHT_WHITE_SPACE if (row + col) % 2 == 0 else self.HIGHLIGHT_BLACK_SPACE
                # Highlight the king if in check
                piece = board.get_piece(row, col)
                if (piece and piece.checkable 
                    and piece.is_in_check(board, (row,col))
                ):
                    color = self.CHECK_COLOR

                rect = (self.BOARD_TOP_LEFT[0] + (col * self.SQUARE_HEIGHT),
                        self.BOARD_TOP_LEFT[1] + (row * self.SQUARE_WIDTH), 
                        self.SQUARE_HEIGHT, self.SQUARE_WIDTH)

                pygame.draw.rect(self.screen, color, rect)

                # Get the piece at the current position
                piece = board.get_piece(row, col)

                # If there is a piece, draw its icon
                if piece:
                    if piece.color == W:
                        icon = piece.WHITE_ICON 
                        
                    elif piece.color == B:
                        icon = piece.BLACK_ICON
                    else:
                        raise ValueError(f'Invalid piece color: {type(piece)} {piece.color}')
                    icon = pygame.transform.scale(icon, (self.SQUARE_WIDTH, self.SQUARE_HEIGHT))
                    self.screen.blit(icon, rect)

