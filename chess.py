from chess_game import ChessGame
from chess_board import ChessBoard

from pieces.pawn import Pawn
from pieces.king import King
from pieces.knight import Knight
from pieces.queen import Queen
from pieces.bishop import Bishop
from pieces.rook import Rook
from teams import W, B

from ai.ai_random import ai_move

game = ChessGame(white_ai=ai_move)
# board = ChessBoard()
# board.set_board([
#     [None,       None,       None,       None,       King(B),    None,       None,       None],
#     [None,       None,       Pawn(W),    None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       None,       None,       None,       None],
#     [None,       None,       None,       None,       King(W),    None,       None,       None],
# ])
# game.set_board(board)
game.start()
