from teams import W, B, other_team
from chess_move import ChessMove
from chess_board import ChessBoard

from pieces.pawn import Pawn
from pieces.king import King
from pieces.knight import Knight
from pieces.queen import Queen
from pieces.bishop import Bishop
from pieces.rook import Rook

from chess_display import ChessDisplay

import pygame
from chess_board import ChessBoard
import sys

from copy import deepcopy

class ChessGameOver:
    """Class for holding information about how a chess game ended"""

    CHECKMATE = "Checkmate"
    STALEMATE = "Stalemate"
    RESIGNATION = "Resignation"
    NOT_OVER = "Game not over"
    FIFTY_MOVE_RULE = "Fifty move rule"
    INSUFFICIENT_MATERIAL = "Insufficient checkmating material"
    
    game_over_type = NOT_OVER
    winning_team = None
    stalemated_team = None

    def set_game_over_type(self, game_over_type):
        """What ended the game"""
        self.game_over_type = game_over_type

    def message(self) -> str:
        """The message to display for this game over"""
        return self.game_over_type


class ChessGame:
    """
    Main Chess Game Class
    
    You can initialize with a white_ai and black_ai.

    The AIs are functions that take in the game object and call the move() function on it. 
    """
    def __init__(self, white_ai = None, black_ai = None):
        self.team_to_move = W
        self.selected_piece = None
        self.selected_position = None
        self.white_ai = white_ai
        self.black_ai = black_ai
        self.board = ChessBoard()
        self.board.set_board([
            [Rook(B),  Knight(B),  Bishop(B),   Queen(B),    King(B),  Bishop(B),  Knight(B),    Rook(B)],
            [Pawn(B),    Pawn(B),    Pawn(B),    Pawn(B),    Pawn(B),    Pawn(B),    Pawn(B),    Pawn(B)],
            [None,       None,       None,       None,       None,       None,       None,       None],
            [None,       None,       None,       None,       None,       None,       None,       None],
            [None,       None,       None,       None,       None,       None,       None,       None],
            [None,       None,       None,       None,       None,       None,       None,       None],
            [Pawn(W),    Pawn(W),    Pawn(W),    Pawn(W),    Pawn(W),    Pawn(W),    Pawn(W),    Pawn(W)],
            [Rook(W),  Knight(W),  Bishop(W),   Queen(W),    King(W),  Bishop(W),  Knight(W),    Rook(W)],
        ])
        self.display = None

    def start(self):
        """
        Start the game. 
        
        This takes control, drawing to the screen and handling input until the game is over.
        """
        self.display = ChessDisplay(self.board)
        # Main game loop
        while True:
            for event in pygame.event.get():
                # Quit the Game
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                # Game is over
                game_over_data = self.get_game_over()
                if game_over_data:
                    self.__on_game_over(game_over_data)
                # Player makes a move
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if self.team_to_move == W and not self.white_ai:
                        self.__on_click()
                    elif self.team_to_move == B and not self.black_ai:
                        self.__on_click()
                # AI makes a move
                if self.team_to_move == W and self.white_ai:
                    self.white_ai(self)
                elif self.team_to_move == B and self.black_ai:
                    self.black_ai(self)
            # Draw the board
            self.display.draw_board(self.board, self.selected_position)
            pygame.display.flip()

    def move(self, move: ChessMove):
        """Make a move"""
        self.board = move.board
        self.team_to_move = other_team(self.team_to_move)
        # Update all the pieces to tell them a move happened
        for i in range(self.board.size()[0]):
            for j in range(self.board.size()[1]):
                piece = self.board.get_piece(i,j)
                if piece:
                    piece.on_move(self.board, move.moved_piece, move.start, move.end)

    def set_board(self, board: ChessBoard):
        """Sets a new board position"""
        self.board = board

    def set_team_to_move(self, team: str):
        """Set whose turn it is"""
        self.team_to_move = team

    def get_board(self) -> ChessBoard:
        """Get the current board position"""
        return self.board

    def get_team_to_move(self) -> str:
        """Gets whose turn it is"""
        return self.team_to_move

    def get_current_moves(self) -> list[ChessMove]:
        """Get a list of all the current legal moves"""
        moves = []
        for i in range(self.board.size()[0]):
            for j in range(self.board.size()[1]):
                piece = self.board.get_piece(i, j)
                if piece and piece.color == self.team_to_move:
                    moves += [move for move in piece.get_legal_moves(self.board, (i, j))]
        return moves

    def get_game_over(self) -> ChessGameOver | None:
        """Get if the game is over"""
        for team in [W, B]:
            if self.board.checkmated(self.team_to_move):
                game_over = ChessGameOver()
                game_over.game_over_type = ChessGameOver.CHECKMATE
                game_over.winning_team = other_team(team)
                return game_over

            if self.board.stalemated(self.team_to_move):
                game_over = ChessGameOver()
                game_over.game_over_type = ChessGameOver.STALEMATE
                game_over.stalemated_team = team
                return game_over
        return None

    def __on_click(self):
        """The player clicked on the board and it is their turn."""
        mouse_pos = pygame.mouse.get_pos()
        clicked_position = self.display.board_pos(mouse_pos)
        if not clicked_position:
            return
        clicked_piece = self.board.get_piece(clicked_position[0], clicked_position[1])
        if not self.selected_piece:
            if clicked_piece and clicked_piece.color == self.team_to_move:
                self.selected_piece = clicked_piece
                self.selected_position = clicked_position
            return
        if isinstance(self.selected_piece, Pawn):
            moves = self.selected_piece.get_legal_moves(self.board, self.selected_position, False)
        else: 
            moves = self.selected_piece.get_legal_moves(self.board, self.selected_position)
        move = [move for move in moves if move.end == clicked_position]
        move: ChessMove = move[0] if len(move) > 0 else None
        if move:
            self.move(move)
        self.selected_piece = None
        self.selected_position = None

    def __on_game_over(self, game_over_data: ChessGameOver):
        """The game is over"""
        self.display.draw_board(self.board)
        self.display.display_message(game_over_data.message())
        pygame.display.flip()
        # Wait for the user to press Q to quit
        waiting_for_quit = True
        while waiting_for_quit:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if k in ['display']:
                setattr(result, k, v)  # shallow copy for the display
            else:
                setattr(result, k, deepcopy(v, memo))
        return result
        